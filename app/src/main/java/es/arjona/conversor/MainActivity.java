package es.arjona.conversor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String DEBUG_TAG = "log";

    @Override
    protected void onCreate(Bundle savedInstanceState) { ;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI(){
        final EditText etinput = findViewById(R.id.et_input);
        final EditText etResultado = findViewById(R.id.et_Resultado);
        final Button buttonConvertirCmToPulg = findViewById(R.id.button_cmToPulgadas);
        final Button buttonConvertirPulgToCm = findViewById(R.id.button_pulgadasToCm);
        final TextView mensajeError = findViewById(R.id.mensaje_error);


        buttonConvertirCmToPulg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    etResultado.setText(convertirdeCmToPulgadas(etinput.getText().toString()));
                } catch (Exception e) {
                    Log.e("LogsConversor",e.getMessage());
                }
                Log.e(DEBUG_TAG, "Vas a convertir de centimetros a pulgadas"); //Comento con un log la funcionalidad del boton

                if (Double.parseDouble(convertirdeCmToPulgadas(etinput.getText().toString())) <=0) {
                    mensajeError.setVisibility(View.VISIBLE);
                }
            }
        });

        buttonConvertirPulgToCm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    etResultado.setText(convertirdePulgadasToCm(etinput.getText().toString()));
                } catch (Exception e) {
                    Log.e("LogsConversor",e.getMessage());
                }
                Log.e(DEBUG_TAG,"Vas a convertir de pulgadas a centimetros"); //Comento con un log la funcionalidad del boton
                if (Double.parseDouble(convertirdePulgadasToCm(etinput.getText().toString())) <=0) {
                    mensajeError.setVisibility(View.VISIBLE);
                }
            }
        });
    }
    //He cambiado los metodos para poder hacer la conversion en ambos sentidos
    private String convertirdePulgadasToCm(String cmText){

        double cmValue = 0;
        //Añadimos los try catch que usaremos para utilizar solo numeros positivos
        try {
            if (Double.parseDouble(cmText) >= 1) {
                 cmValue = Double.parseDouble(cmText) / 2.54;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("SOLO ADMITIDOS NUMEROS MAYORES O IGUALES A UNO");
        }
        return String.valueOf(cmValue);
    }

    private String convertirdeCmToPulgadas(String pulgadaText) {
        double cmValue = 0;

        try {
            if (Double.parseDouble(pulgadaText) >= 1) {
                double pulgadaValue = Double.parseDouble(pulgadaText) * 2.54;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("SOLO ADMITIDOS NUMEROS MAYORES O IGUALES A UNO");

        }
        return String.valueOf(cmValue);
    }

    private String dspsi(double cmText) {
        double cmValue = 0;

        try {
            if (cmText >= 1) {
                cmValue = cmText / 2.54;

                return String.valueOf(cmValue);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("SOLO ADMITIDOS NUMEROS MAYORES O IGUALES A UNO");

        }
        return String.valueOf(cmValue);
    }

}